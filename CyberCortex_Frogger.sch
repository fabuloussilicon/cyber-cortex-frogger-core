<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="O_VIDEO_R(3:0)" />
        <signal name="O_VIDEO_G(3:0)" />
        <signal name="O_VIDEO_B(3:0)" />
        <signal name="O_LED(3:0)" />
        <signal name="O_HSYNC" />
        <signal name="O_VSYNC" />
        <signal name="O_AUDIO_L" />
        <signal name="O_AUDIO_R" />
        <signal name="I_SW(3:0)" />
        <signal name="I_RESET" />
        <signal name="I_BUTTON(3:0)" />
        <signal name="clk" />
        <signal name="XLXN_13" />
        <port polarity="Output" name="O_VIDEO_R(3:0)" />
        <port polarity="Output" name="O_VIDEO_G(3:0)" />
        <port polarity="Output" name="O_VIDEO_B(3:0)" />
        <port polarity="Output" name="O_LED(3:0)" />
        <port polarity="Output" name="O_HSYNC" />
        <port polarity="Output" name="O_VSYNC" />
        <port polarity="Output" name="O_AUDIO_L" />
        <port polarity="Output" name="O_AUDIO_R" />
        <port polarity="Input" name="I_SW(3:0)" />
        <port polarity="Input" name="I_RESET" />
        <port polarity="Input" name="I_BUTTON(3:0)" />
        <port polarity="Input" name="clk" />
        <blockdef name="SCRAMBLE_TOP">
            <timestamp>2011-4-22T2:49:29</timestamp>
            <rect width="496" x="64" y="-1280" height="1280" />
            <line x2="0" y1="-1248" y2="-1248" x1="64" />
            <line x2="0" y1="-848" y2="-848" x1="64" />
            <rect width="64" x="0" y="-460" height="24" />
            <line x2="0" y1="-448" y2="-448" x1="64" />
            <rect width="64" x="0" y="-60" height="24" />
            <line x2="0" y1="-48" y2="-48" x1="64" />
            <line x2="624" y1="-1248" y2="-1248" x1="560" />
            <line x2="624" y1="-1184" y2="-1184" x1="560" />
            <line x2="624" y1="-1120" y2="-1120" x1="560" />
            <line x2="624" y1="-1056" y2="-1056" x1="560" />
            <line x2="624" y1="-992" y2="-992" x1="560" />
            <line x2="624" y1="-928" y2="-928" x1="560" />
            <line x2="624" y1="-864" y2="-864" x1="560" />
            <line x2="624" y1="-800" y2="-800" x1="560" />
            <line x2="624" y1="-736" y2="-736" x1="560" />
            <line x2="624" y1="-672" y2="-672" x1="560" />
            <line x2="624" y1="-608" y2="-608" x1="560" />
            <line x2="624" y1="-544" y2="-544" x1="560" />
            <line x2="624" y1="-480" y2="-480" x1="560" />
            <line x2="624" y1="-416" y2="-416" x1="560" />
            <rect width="64" x="560" y="-364" height="24" />
            <line x2="624" y1="-352" y2="-352" x1="560" />
            <rect width="64" x="560" y="-300" height="24" />
            <line x2="624" y1="-288" y2="-288" x1="560" />
            <rect width="64" x="560" y="-236" height="24" />
            <line x2="624" y1="-224" y2="-224" x1="560" />
            <rect width="64" x="560" y="-172" height="24" />
            <line x2="624" y1="-160" y2="-160" x1="560" />
            <rect width="64" x="560" y="-108" height="24" />
            <line x2="624" y1="-96" y2="-96" x1="560" />
            <rect width="64" x="560" y="-44" height="24" />
            <line x2="624" y1="-32" y2="-32" x1="560" />
        </blockdef>
        <blockdef name="Scramble_Clock">
            <timestamp>2014-7-15T5:37:46</timestamp>
            <rect width="336" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="464" y1="-224" y2="-224" x1="400" />
            <line x2="464" y1="-160" y2="-160" x1="400" />
            <line x2="464" y1="-96" y2="-96" x1="400" />
            <line x2="464" y1="-32" y2="-32" x1="400" />
        </blockdef>
        <block symbolname="SCRAMBLE_TOP" name="XLXI_1">
            <blockpin signalname="I_RESET" name="I_RESET" />
            <blockpin signalname="XLXN_13" name="I_CLK_REF" />
            <blockpin signalname="I_SW(3:0)" name="I_SW(3:0)" />
            <blockpin signalname="I_BUTTON(3:0)" name="I_BUTTON(3:0)" />
            <blockpin name="O_STRATAFLASH_CE_L" />
            <blockpin name="O_STRATAFLASH_OE_L" />
            <blockpin name="O_STRATAFLASH_WE_L" />
            <blockpin name="O_STRATAFLASH_BYTE" />
            <blockpin name="O_LCD_RW" />
            <blockpin name="O_LCD_E" />
            <blockpin name="O_SPI_ROM_CS" />
            <blockpin name="O_SPI_ADC_CONV" />
            <blockpin name="O_SPI_DAC_CS" />
            <blockpin name="O_PLATFORMFLASH_OE" />
            <blockpin signalname="O_HSYNC" name="O_HSYNC" />
            <blockpin signalname="O_VSYNC" name="O_VSYNC" />
            <blockpin signalname="O_AUDIO_L" name="O_AUDIO_L" />
            <blockpin signalname="O_AUDIO_R" name="O_AUDIO_R" />
            <blockpin name="O_STRATAFLASH_ADDR(23:0)" />
            <blockpin signalname="O_VIDEO_R(3:0)" name="O_VIDEO_R(3:0)" />
            <blockpin signalname="O_VIDEO_G(3:0)" name="O_VIDEO_G(3:0)" />
            <blockpin signalname="O_VIDEO_B(3:0)" name="O_VIDEO_B(3:0)" />
            <blockpin signalname="O_LED(3:0)" name="O_LED(3:0)" />
            <blockpin name="B_STRATAFLASH_DATA(7:0)" />
        </block>
        <block symbolname="Scramble_Clock" name="XLXI_2">
            <blockpin signalname="clk" name="CLKIN_IN" />
            <blockpin name="LOCKED_OUT" />
            <blockpin signalname="XLXN_13" name="CLKFX_OUT" />
            <blockpin name="CLKIN_IBUFG_OUT" />
            <blockpin name="CLK0_OUT" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="7040">
        <instance x="2592" y="4208" name="XLXI_1" orien="R0">
        </instance>
        <branch name="O_VIDEO_R(3:0)">
            <wire x2="3248" y1="3920" y2="3920" x1="3216" />
        </branch>
        <iomarker fontsize="28" x="3248" y="3920" name="O_VIDEO_R(3:0)" orien="R0" />
        <branch name="O_VIDEO_G(3:0)">
            <wire x2="3248" y1="3984" y2="3984" x1="3216" />
        </branch>
        <iomarker fontsize="28" x="3248" y="3984" name="O_VIDEO_G(3:0)" orien="R0" />
        <branch name="O_VIDEO_B(3:0)">
            <wire x2="3248" y1="4048" y2="4048" x1="3216" />
        </branch>
        <iomarker fontsize="28" x="3248" y="4048" name="O_VIDEO_B(3:0)" orien="R0" />
        <branch name="O_LED(3:0)">
            <wire x2="3248" y1="4112" y2="4112" x1="3216" />
        </branch>
        <iomarker fontsize="28" x="3248" y="4112" name="O_LED(3:0)" orien="R0" />
        <branch name="O_HSYNC">
            <wire x2="3248" y1="3600" y2="3600" x1="3216" />
        </branch>
        <iomarker fontsize="28" x="3248" y="3600" name="O_HSYNC" orien="R0" />
        <branch name="O_VSYNC">
            <wire x2="3248" y1="3664" y2="3664" x1="3216" />
        </branch>
        <iomarker fontsize="28" x="3248" y="3664" name="O_VSYNC" orien="R0" />
        <branch name="O_AUDIO_L">
            <wire x2="3248" y1="3728" y2="3728" x1="3216" />
        </branch>
        <iomarker fontsize="28" x="3248" y="3728" name="O_AUDIO_L" orien="R0" />
        <branch name="O_AUDIO_R">
            <wire x2="3248" y1="3792" y2="3792" x1="3216" />
        </branch>
        <iomarker fontsize="28" x="3248" y="3792" name="O_AUDIO_R" orien="R0" />
        <branch name="I_SW(3:0)">
            <wire x2="2592" y1="3760" y2="3760" x1="2560" />
        </branch>
        <iomarker fontsize="28" x="2560" y="3760" name="I_SW(3:0)" orien="R180" />
        <branch name="I_RESET">
            <wire x2="2592" y1="2960" y2="2960" x1="2560" />
        </branch>
        <iomarker fontsize="28" x="2560" y="2960" name="I_RESET" orien="R180" />
        <branch name="I_BUTTON(3:0)">
            <wire x2="2592" y1="4160" y2="4160" x1="2560" />
        </branch>
        <iomarker fontsize="28" x="2560" y="4160" name="I_BUTTON(3:0)" orien="R180" />
        <branch name="clk">
            <wire x2="1632" y1="3344" y2="3344" x1="1600" />
        </branch>
        <iomarker fontsize="28" x="1600" y="3344" name="clk" orien="R180" />
        <branch name="XLXN_13">
            <wire x2="2336" y1="3408" y2="3408" x1="2096" />
            <wire x2="2336" y1="3360" y2="3408" x1="2336" />
            <wire x2="2592" y1="3360" y2="3360" x1="2336" />
        </branch>
        <instance x="1632" y="3568" name="XLXI_2" orien="R0">
        </instance>
    </sheet>
</drawing>